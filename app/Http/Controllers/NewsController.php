<?php

namespace App\Http\Controllers;

use App\News;
 use Carbon\Carbon;
 use Image;
use App\Http\Requests\NewsRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $sl = !is_null(\request()->page) ? (\request()->page -1 )* 10 : 0;

        $news = News::orderBy('created_at', 'desc')->paginate(10);

        return view('backend.news.index', compact('news', 'sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

        return view('backend.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
      
        try {        
           

            News::create($request->all());
            return redirect()->route('news.index')->withStatus('Created Successfully !');
        }catch (QueryException $e){
          return redirect()->back()->withInput()->withErrors($e->getMessage());
    

        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(News $new) //route model model binding/ dependency injection
    {

        return view('backend.news.show', compact('new'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $new)
    {
//        $category = Category::findOrFail($id);

        return view('backend.news.edit', compact('new'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsRequest $request, News $new)
    {
        try{
            $new->update($request->all());
            return redirect()->route('news.index')->withStatus('Updated Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $new)
    {
        try{
            $new->delete();
            return redirect()->route('news.index')->withStatus('Deleted Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        $sl = !is_null(\request()->page) ? (\request()->page -1 )* 10 : 0;

        $news = News::onlyTrashed()->paginate(10);
        return view('backend.news.trash', compact('news', 'sl'));
    }

    public function showTrash($id) 
    {


        $new = News::onlyTrashed()
                            ->where('id', $id)
                            ->first();



        return view('backend.news.show', compact('new'));
    }

    public function restoreTrash($id)
    {
        try{

            $new = News::onlyTrashed()
                ->where('id', $id)
                ->first();

            $new->restore();

            return redirect()->route('news.trash')->withStatus('Updated Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function deleteTrash($id)
    {
        try{
            $new = News::onlyTrashed()
                ->where('id', $id)
                ->first();

            $new->forceDelete();

            return redirect()->route('news.trash')->withStatus('Deleted Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }




}

    

