<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use PDF;

class PdfController extends Controller
{
	public function profiles()
    {

        $profiles = Profile::orderBy('created_at', 'desc')->get();

        $pdf = PDF::loadView('backend.profiles.pdf', compact('profiles'));
        return $pdf->download('profiles.pdf');
    }
    
}
