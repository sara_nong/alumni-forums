<?php

namespace App\Http\Controllers;
use App\Alumni;
// use App\Exports\CategoryExport;
use App\Http\Requests\AlumniRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AlumniController extends Controller
{
    
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $sl = !is_null(\request()->page) ? (\request()->page -1 )* 10 : 0;

        $profiles = Profile::orderBy('created_at', 'desc')->paginate(10);

        return view('backend.profiles.index', compact('profiles', 'sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

        $profiles =Profile::pluck('product_title','id')
        return view('backend.profiles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileRequest $request)
    {
      
        try {        
           

            Profile::create($request->all());
            return redirect()->route('profiles.index')->withStatus('Created Successfully !');
        }catch (QueryException $e){
          return redirect()->back()->withInput()->withErrors($e->getMessage());
    

        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile) //route model model binding/ dependency injection
    {

        return view('backend.profiles.show', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
//        $category = Category::findOrFail($id);

        return view('backend.profiles.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request, Profile $profile)
    {
        try{
            $profile->update($request->all());
            return redirect()->route('profiles.index')->withStatus('Updated Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        try{
            $profile->delete();
            return redirect()->route('profiles.index')->withStatus('Deleted Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        $sl = !is_null(\request()->page) ? (\request()->page -1 )* 10 : 0;

        $profiles = Product::onlyTrashed()->paginate(10);
        return view('backend.profiles.trash', compact('profiles', 'sl'));
    }

    public function showTrash($id) 
    {


        $product = Product::onlyTrashed()
                            ->where('id', $id)
                            ->first();



        return view('backend.profiles.show', compact('product'));
    }

    public function restoreTrash($id)
    {
        try{

            $product = Product::onlyTrashed()
                ->where('id', $id)
                ->first();

            $product->restore();

            return redirect()->route('profiles.trash')->withStatus('Updated Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function deleteTrash($id)
    {
        try{
            $product = Product::onlyTrashed()
                ->where('id', $id)
                ->first();

            $product->forceDelete();

            return redirect()->route('profiles.trash')->withStatus('Deleted Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

}

