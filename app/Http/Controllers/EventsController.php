<?php

namespace App\Http\Controllers;

use App\Event;
use Carbon\Carbon;
use Image;
use App\Http\Requests\EventRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $sl = !is_null(\request()->page) ? (\request()->page -1 )* 10 : 0;

        $events = Event::orderBy('created_at', 'desc')->paginate(10);

        return view('backend.events.index', compact('events', 'sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

        return view('backend.events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
      
        try {        
           

            Event::create($request->all());
            return redirect()->route('events.index')->withStatus('Created Successfully !');
        }catch (QueryException $e){
          return redirect()->back()->withInput()->withErrors($e->getMessage());
    

        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event) //route model model binding/ dependency injection
    {

        return view('backend.events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
//        $category = Category::findOrFail($id);

        return view('backend.events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, Event $event)
    {
        try{
            $event->update($request->all());
            return redirect()->route('events.index')->withStatus('Updated Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        try{
            $event->delete();
            return redirect()->route('events.index')->withStatus('Deleted Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        $sl = !is_null(\request()->page) ? (\request()->page -1 )* 10 : 0;

        $events = Event::onlyTrashed()->paginate(10);
        return view('backend.events.trash', compact('events', 'sl'));
    }

    public function showTrash($id) 
    {


        $event = Event::onlyTrashed()
                            ->where('id', $id)
                            ->first();



        return view('backend.events.show', compact('event'));
    }

    public function restoreTrash($id)
    {
        try{

            $event = Event::onlyTrashed()
                ->where('id', $id)
                ->first();

            $event->restore();

            return redirect()->route('events.trash')->withStatus('Updated Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function deleteTrash($id)
    {
        try{
            $event = Event::onlyTrashed()
                ->where('id', $id)
                ->first();

            $event->forceDelete();

            return redirect()->route('events.trash')->withStatus('Deleted Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }




}

    

