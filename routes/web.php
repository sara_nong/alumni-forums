<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'FrontendController@welcome')->name('welcome');



Route::get('/about', function () {
    return view('frontend.about');
})->name('about');
Route::get('/blog-home', function () {
    return view('frontend/blog-home');
})->name('blog-home');
Route::get('/blog-single', function () {
    return view('frontend/blog-single');
})->name('blog-single');
Route::get('/gallery', function () {
    return view('frontend/gallery');
})->name('gallery');
Route::get('/events', function () {
    return view('frontend/events');
})->name('events');

Route::get('/garo', function () {
    return view('frontend/garo');
})->name('garo');


Route::get('/event-details', function () {
    return view('frontend/event-details');
})->name('event-details');

Route::get('/contact', function () {
    return view('frontend/contact');
})->name('contact');


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {

Route::get('/', function (){
    return view('backend.dashboard');
    })->name('home');





    Route::get('/my-profile', 'UserController@myProfile')->name('my_profile');
    Route::get('/users/{id}/edit', 'UserController@edit')->name('users.edit');
    Route::put('/users/{id}', 'UserController@update')->name('users.update');

 Route::get('/profiles/pdf', 'PdfController@profiles')->name('profiles.pdf');

     Route::get('/profiles/excel', 'ExcelController@profiles')->name('profiles.excel');
 Route::get('/profiles/trash', 'ProfileController@trash')->name('profiles.trash');
    Route::get('/profiles/trash/{id}', 'ProfileController@showTrash')->name('profiles.show_trash');
    Route::put('/profiles/trash/{id}', 'ProfileController@restoreTrash')->name('profiles.restore_trash');
    Route::delete('/profiles/trash/{id}', 'ProfileController@deleteTrash')->name('profiles.delete_trash');

Route::get('/profiles','ProfileController@index')->name('profiles.index');
Route::get('/profiles/create', 'ProfileController@create')->name('profiles.create');

Route::post('/profiles', 'ProfileController@store')->name('profiles.store');

 Route::get('/profiles/{profile}','ProfileController@show')->name('profiles.show');
Route::get('/profiles/{profile}/edit', 'ProfileController@edit')->name('profiles.edit');
Route::put('/profiles/{profile}', 'ProfileController@update')->name('profiles.update');
Route::delete('/profiles/{profile}', 'ProfileController@destroy')->name('profiles.destroy');

Route::get('/news/trash', 'NewsController@trash')->name('news.trash');
    Route::get('/news/trash/{id}', 'NewsController@showTrash')->name('news.show_trash');
    Route::put('/news/trash/{id}', 'NewsController@restoreTrash')->name('news.restore_trash');
    Route::delete('/news/trash/{id}', 'NewsController@deleteTrash')->name('news.delete_trash');

    Route::get('/news','NewsController@index')->name('news.index');
Route::get('/news/create', 'NewsController@create')->name('news.create');

Route::post('/news', 'NewsController@store')->name('news.store');

 Route::get('/news/{new}','NewsController@show')->name('news.show');
Route::get('/news/{new}/edit', 'NewsController@edit')->name('news.edit');
Route::put('/news/{new}', 'NewsController@update')->name('news.update');
Route::delete('/news/{new}', 'NewsController@destroy')->name('news.destroy');
     route::resource('events','EventsController');
Route::get('/my-profile', 'UserController@myProfile')->name('my_profile');
    Route::get('/users/{id}/edit', 'UserController@edit')->name('users.edit');
    Route::put('/users/{id}', 'UserController@update')->name('users.update');


Route::get('/jobs/trash', 'JobsController@trash')->name('jobs.trash');
    Route::get('/jobs/trash/{id}', 'JobsController@showTrash')->name('jobs.show_trash');
    Route::put('/jobs/trash/{id}', 'JobsController@restoreTrash')->name('jobs.restore_trash');
    Route::delete('/jobs/trash/{id}', 'JobsController@deleteTrash')->name('jobs.delete_trash');
     route::resource('jobs','JobsController');

     route::resource('garos','GaroController');



});