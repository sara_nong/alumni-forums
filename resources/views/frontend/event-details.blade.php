	@extends('frontend.layouts.master')

@section('title', 'Event')

@section('content')
			<!-- start banner Area -->
			<section class="banner-area relative about-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Event Details			
							</h1>	
							<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="event-details.html"> Event Details</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	
				
			<!-- Start event-details Area -->
			<section class="event-details-area section-gap">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 event-details-left">
							<div class="main-img">
								<img class="img-fluid" src="{{asset('ui/frontend')}}/img/events/2.jpg" alt="">
							</div>
							<div class="details-content">
								<a href="#">
									<h4>Parents Day</h4>
								</a>
								<p>
									To strengthen the triangular relationship of parents-students and teachers, Daffodil International University (DIU) organized a program under the course of 'Art of Living' with parents titled "Parents Day" held at DIU Permanent Campus yesterday (Friday) on October 18, 2019.									
								</p>
								<p>
									Directorate of Student Affairs of Daffodil International University organized the program as an essential part of Art of Living course where Dr. Md. Sabur Khan, Chairman; Board of Trustees of Daffodil International University was present as the chief guest. Presided over by Vice Chancellor of DIU, Professor Dr. Yousuf Mahabubul Islam, the program was also addressed by Professor Dr. Mostafa Kamal, Dean (Permanent Campus) and  Mr. Mahabub Parvez, Head, Department of Tourism and Hospitality Management, Mr. Syed Mizanur Rahman Raju, Director of Student Affair's conducted the interactive program where a large number of parents and guardians took part and exchanged their views. Around 3000 students, parents and guardians took part in the day long program
The day long program started with national anthem assembled all together. Then the students took formal oath to keep their parents always along with beside them and they would never send them to old home.									
								</p>
							</div>
							<div class="social-nav row no-gutters">
								<div class="col-lg-6 col-md-6 ">
									<ul class="focials">
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										<li><a href="#"><i class="fa fa-behance"></i></a></li>
									</ul>
								</div>
								<div class="col-lg-6 col-md-6 navs">
									<a href="#" class="nav-prev"><span class="lnr lnr-arrow-left"></span>Prev Event</a>
									<a href="#" class="nav-next">Next Event<span class="lnr lnr-arrow-right"></span></a>									
								</div>
							</div>
						</div>
						<div class="col-lg-4 event-details-right">
							<div class="single-event-details">
								<h4>Details</h4>
								<ul class="mt-10">
									<li class="justify-content-between d-flex">
										<span>Start date</span>
										<span>15th April, 2018</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>End date</span>
										<span>18th April, 2018</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>Ticket Price</span>
										<span>Free of Cost</span>
									</li>														
								</ul>
							</div>
							<div class="single-event-details">
								<h4>Venue</h4>
								<ul class="mt-10">
									<li class="justify-content-between d-flex">
										<span>Place</span>
										<span>Swadhinota Hall</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>Street</span>
										<span>Ashulia</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>City</span>
										<span>Dhaka</span>
									</li>														
								</ul>
							</div>
							<div class="single-event-details">
								<h4>Organiser</h4>
								<ul class="mt-10">
									<li class="justify-content-between d-flex">
										<span>Company</span>
										<span>Daffodil International University</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>Street</span>
										<span>Ashulia,Savar</span>
									</li>
									<li class="justify-content-between d-flex">
										<span>City</span>
										<span>Dhaka</span>
									</li>														
								</ul>
							</div>														
						</div>
					</div>
				</div>	
			</section>
			<!-- End event-details Area -->
					
				
									    			

			@endsection