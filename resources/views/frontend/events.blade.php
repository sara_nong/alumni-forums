	@extends('frontend.layouts.master')

@section('title', 'Events')

@section('content')
			<!-- start banner Area -->
			<section class="banner-area relative about-banner" id="home">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Events				
							</h1>	
							<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="events.html"> Events</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->	
				
			<!-- Start events-list Area -->
			<section class="events-list-area section-gap event-page-lists">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-6 pb-30">
							<div class="single-carusel row align-items-center">
								<div class="col-12 col-md-6 thumb">
									<img class="img-fluid" src="{{asset('ui/frontend')}}/img//events/1.jpg" alt="">
								</div>
								<div class="detials col-12 col-md-6">
									<a href="{{url('event-details')}}"><h4>											Reunion Program with Chairman Sir
</h4></a>
									<p>25th February, 2019</p>
									<p>
										For most of us, the idea of astronomy is something we directly connect to “stargazing”, telescopes and seeing magnificent displays in the heavens.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 pb-30">
							<div class="single-carusel row align-items-center">
								<div class="col-12 col-md-6 thumb">
									<img class="img-fluid" src="{{asset('ui/frontend')}}/img/events/2.jpg" alt="">
								</div>
								<div class="detials col-12 col-md-6">
									<a href="{{url('event-details')}}"><h4>
												Parents Day
</h4></a>
									<p>22th December, 2019</p>
									<p>
										For most of us, the idea of astronomy is something we directly connect to “stargazing”, telescopes and seeing magnificent displays in the heavens.
									</p>
								</div>
							</div>
						</div>
						
						
						<div class="col-lg-6 pb-30">
							<div class="single-carusel row align-items-center">
								<div class="col-12 col-md-6 thumb">
									<img class="img-fluid" src="{{asset('ui/frontend')}}/img/events/3.jpg" alt="">
								</div>
								<div class="detials col-12 col-md-6">
									
									<a href="{{url('event-details')}}"><h4>
									Blood Donation Camp</h4></a>
									<p>25th February, 2018</p>
									<p>
										For most of us, the idea of astronomy is something we directly connect to “stargazing”, telescopes and seeing magnificent displays in the heavens.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-6" pb-30>
							<div class="single-carusel row align-items-center">
								<div class="col-12 col-md-6 thumb">
									<img class="img-fluid" src="{{asset('ui/frontend')}}/img/events/6.jpg" alt="">
								</div>
								<div class="detials col-12 col-md-6">
									
									<a href="{{url('event-details')}}"><h4>Alumni Reunion</h4></a>
									<p>25th February, 2018</p>
									<p>
										For most of us, the idea of astronomy is something we directly connect to “stargazing”, telescopes and seeing magnificent displays in the heavens.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-6 pb-30">
							<div class="single-carusel row align-items-center">
								<div class="col-12 col-md-6 thumb">
									<img class="img-fluid" src="{{asset('ui/frontend')}}/img/events/8.jpg" alt="">
								</div>
								<div class="detials col-12 col-md-6">
									<a href="{{url('event-details')}}"><h4>Sports at Permanent Campus</h4></a>
									<p>25th February, 2018</p>
									<p>
										For most of us, the idea of astronomy is something we directly connect to “stargazing”, telescopes and seeing magnificent displays in the heavens.
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="single-carusel row align-items-center">
								<div class="col-12 col-md-6 thumb">
									<img class="img-fluid" src="{{asset('ui/frontend')}}/img/events/7.jpg" alt="">
								</div>
								<div class="detials col-12 col-md-6">
									<a href="{{url('event-details')}}"><h4>The Universe Through
									A Child S Eyes</h4></a>
									<p>25th February, 2018</p>
									<p>
										For most of us, the idea of astronomy is something we directly connect to “stargazing”, telescopes and seeing magnificent displays in the heavens.
									</p>
								</div>
							</div>
						</div>																		
						<a href="#" class="text-uppercase primary-btn mx-auto mt-40">Load more Events</a>		
					</div>
				</div>	
			</section>
			<!-- End events-list Area -->
				

								    			

			@endsection