@extends('backend.layouts.master')

@section('title', 'Categories')

@section('content')
<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-md-6">Alumni Profile</div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('profiles.create') }}" class="btn btn-sm btn-outline-primary">Add New</a>
                    <a href="{{ route('profiles.trash') }}" class="btn btn-sm btn-outline-danger">Trash List</a>
                </div>
            </div>
        </div>
        
        <div class="card-body">
            <div class="table-responsive" 
            id="dataTable">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>SL#</th>
                        <th>Name</th>
                        <th>Department</th>
                        <th style="width: 150px; text-align: center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(session()->has('status'))
                        <div class="alert alert-success">
                            <p>{{ session('status') }}</p>
                        </div>
                    @endif
                    @foreach($profiles as $profile)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $profile->name }}</td>
                        <td>{{ $profile->department}}</td>
                        <td>
                            <a href="{{ route('profiles.show', $profile->id) }}" class="btn btn-sm btn-outline-info">Show</a>
                            <a href="{{ route('profiles.edit', $profile->id) }}" class="btn btn-sm btn-outline-warning">Edit</a>

{{--                            <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>--}}

                            <form action="{{ route('profiles.destroy', $profile->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" onclick="return confirm('Are You Sure Want To Delete?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
           
        </div>
        <div class="card-footer text-right">
            <a href="{{ route('profiles.excel') }}" class="btn btn-sm btn-outline-info">Download Excel</a>
            <a href="{{ route('profiles.pdf') }}" class="btn btn-sm btn-outline-danger">Download Pdf</a>
        </div>
        
    </div>

</div>
@endsection


