<div class="form-row">
    <div class="col-md-6 mb-3">

        {{ Form::label('title', 'Title') }}


        {{ Form::text('title', null, [
            'class' => $errors->has('title') ?  'form-control is-invalid': 'form-control',
            'placeholder' => 'Enter Title.....',
            'id' => 'title'
        ]) }}
        @error('title')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>


   

   

<div class="col-md-6 mb-3">
        {{ Form::label('time', 'Time') }}


        {{ Form::text('time', null, [
            'class' => $errors->has('description') ?  'form-control is-invalid': 'form-control',
            'placeholder' => 'Enter Time Here.....',
            'id' => 'time',
            
        ]) }}
        @error('time')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

<div class="col-md-6 mb-3">
        {{ Form::label('date', 'Date') }}


        {{ Form::date('date', null, [
            'class' => $errors->has('date') ?  'form-control is-invalid': 'form-control',
            'placeholder' => 'Enter Date Here.....',
            'id' => 'date',
            
        ]) }}
        @error('date')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>




</div>




