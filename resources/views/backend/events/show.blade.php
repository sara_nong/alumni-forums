@extends('backend.layouts.master')

@section('title', 'news Details')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Show news</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('events.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Title</th>
                            <th>{{ $event->title }}</th>
                        </tr>
                        
                     
<tr>
    <th>Time</th>
<th>{{$event->time}}</th>
</tr>
<tr>
    <th>Date</th>
<th>{{$event->date}}</th>
</tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

