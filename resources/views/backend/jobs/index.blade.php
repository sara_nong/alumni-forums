@extends('backend.layouts.master')

@section('title', 'jobs')

@section('content')
<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-md-6">Jobs List</div>

                <div class="col-md-6 text-right">
                    <a href="{{ route('jobs.create') }}" class="btn btn-sm btn-outline-primary">Add New</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive"
             id="dataTable">
                             <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>SL#</th>
                        <th>Job Title</th>
                       
                        <th style="width: 150px; text-align: center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(session()->has('status'))
                        <div class="alert alert-success">
                            <p>{{ session('status') }}</p>
                        </div>
                    @endif
                    @foreach($jobs as $job)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $job->title }}</td>
                      
                        <td>
                            <a href="{{ route('jobs.show', $job->id) }}" class="btn btn-sm btn-outline-info">Show</a>
                            <a href="{{ route('jobs.edit', $job->id) }}" class="btn btn-sm btn-outline-warning">Edit</a>

{{--                            <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>--}}

                            <form action="{{ route('jobs.destroy', $job->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" onclick="return confirm('Are You Sure Want To Delete?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $jobs->links() }}
        </div>
       
        
    </div>

</div>
@endsection


