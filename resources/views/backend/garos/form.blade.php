<div class="form-row">

    <div class="col-md-12 mb-3">
        {{ Form::label('image', ' Image') }}<br>

        {{ Form::file('image', ['accept'=>'image/*'], [
            'class' => $errors->has('image') ?  'form-control is-invalid': 'form-control',
            'id' => 'image'
        ]) }}
        @error('image')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>



    <div class="col-md-12 mb-3">

        {{ Form::label('name', 'Name') }}


        {{ Form::text('name', null, [
            'class' => $errors->has('name') ?  'form-control is-invalid': 'form-control',
            'placeholder' => 'Enter Name.....',
            'id' => 'name'
        ]) }}
        @error('name')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

 <div class="col-md-12 mb-3">
        {{ Form::label('university', 'University') }}


        {{ Form::text('university', null, [
            'class' => $errors->has('university') ?  'form-control is-invalid': 'form-control',
            'placeholder' => 'Enter Department...',
            'id' => 'university'
        ]) }}
        @error('university')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="col-md-12 mb-3">
        {{ Form::label('department', 'Department') }}


        {{ Form::text('department', null, [
            'class' => $errors->has('department') ?  'form-control is-invalid': 'form-control',
            'placeholder' => 'Enter Department...',
            'id' => 'department'
        ]) }}
        @error('department')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    
</div>







