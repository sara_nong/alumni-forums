@extends('backend.layouts.master')

@section('title', 'profiles Details')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Show profiles</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('garos.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                    </div>
                </div>
            </div>


            <div class="p-4">
               
                @if(file_exists(storage_path().'/app/public/garos/'.$garo->image ) && (!is_null($garo->image)))
                    <img src="{{ asset('storage/garos/'.$garo->image) }}" height="100">
                @else
                    <img src="{{ asset('/default-avatar.png') }}">
                @endif
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <tbody>



                        <tr>
                            <th>Name</th>
                            <th>{{ $garo->name }}</th>
                        </tr>
                        
                      <tr>
                        <th>University</th>
                        <th>{{$garo->university}}</th>
                    </tr>
                    <tr>
                            <th>Department</th>
                            <th>{{ $garo->department }}</th>
                        </tr>

                        <tr>
    <th>Graduation Year</th>
   
</tr>
<tr>
    <th>Email Address</th>
</tr>
<tr>
    <th>Phone Number</th>
</tr>
<tr><th>Address</th>
</tr> 
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

