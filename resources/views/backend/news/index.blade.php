@extends('backend.layouts.master')

@section('title', 'news')

@section('content')
<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-md-6">Campus News</div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('news.create') }}" class="btn btn-sm btn-outline-primary">Add New</a>
                    <a href="{{ route('news.trash') }}" class="btn btn-sm btn-outline-danger">Trash List</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
{{--                id="dataTable"--}}
                <table class="table table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>SL#</th>
                        <th>Title</th>
                       
                        <th style="width: 150px; text-align: center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(session()->has('status'))
                        <div class="alert alert-success">
                            <p>{{ session('status') }}</p>
                        </div>
                    @endif
                    @foreach($news as $new)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $new->title }}</td>
                        
                        <td>
                            <a href="{{ route('news.show', $new->id) }}" class="btn btn-sm btn-outline-info">Show</a>
                            <a href="{{ route('news.edit', $new->id) }}" class="btn btn-sm btn-outline-warning">Edit</a>

{{--                            <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>--}}

                            <form action="{{ route('news.destroy', $new->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" onclick="return confirm('Are You Sure Want To Delete?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $news->links() }}
        </div>
       
        
    </div>

</div>
@endsection


